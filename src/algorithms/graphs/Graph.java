package algorithms.graphs;

import java.util.*;

/**
 * Represent a graph with multiple edges and vertices.
 * <p/>
 * Requirements of a graph:
 * <p/>
 * 1. One edge can only have 2 vertices
 * <p/>
 * 2. There can be multiple edges coming out of a vertex
 * <p/>
 * <p/>
 * Created by TrevorTa on 6/4/16.
 */
public class Graph {
    private List<Vertex> vertices = new ArrayList<Vertex>();
    private List<Edge> edges = new ArrayList<Edge>();

    /**
     * Create empty graph.
     */
    public Graph() {
        vertices = null;
        edges = null;
    }

    /**
     * Initalize the graph with inputs.
     *
     * @param pVertices:
     *         input vertices
     * @param pEdges:
     *         input edges. Require all edges to be in the vertices
     */
    public Graph(List<Vertex> pVertices, List<Edge> pEdges) {
        vertices = pVertices;
        edges = pEdges;
    }

    public void addVertex(Vertex v) {

    }

    public void addEdge(Edge e) {

    }
}
