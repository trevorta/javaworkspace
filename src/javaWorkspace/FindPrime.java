package javaWorkspace;

import java.util.*;

/**
 * Created by TrevorSF on 3/10/2016.
 */
public class FindPrime {
    public static void main(String[] args) {
        printPrime(10);
    }

    /**
     * A cooler print primes. This method will use the fact that a number is prime if and only if it doesn't divide all
     * primes that are smaller than itself.
     *
     * @param n
     */
    public static void printPrime(int n) {
        List<Integer> previousPrimes = new ArrayList<Integer>();
        int numPrinted = 0;
        int i = 2; // start with the first prime number
        while (numPrinted < n) {
            for (Integer p : previousPrimes) {
                if (i % p == 0) { // if i divides, p continues
                    i++;
                    continue;
                }
            }
            // after checking all, i is a prime, we add i to previousPrimes and continue
            previousPrimes.add(i);
            System.out.println(i);
            i++;
            numPrinted++;
        }
    }

    /**
     * Print @param n number of primes starting at 2.
     *
     * @param n
     */
    public static void printPrimeV2(int n) {
        int numPrimes = 0;
        int i = 2;
        while (numPrimes < n) {
            if (checkPrime(i)) { // if i is a prime number
                System.out.println(i);
                numPrimes++; // found a prime number, move on to the next one
            }
            i++; // increase i every loop
        }
    }

    /**
     * A cooler check prime. This method uses the fact that we don't need to check the numbers that are greater than
     * sqrt(n).
     *
     * @param n
     *
     * @return true if n is a prime number
     */
    public static boolean checkPrime(int n) {
        if (n < 2) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) { // if n divides i, then n is not a prime
                return false;
            }
        }
        return true;
    }

    /**
     * Check if @param n is a prime number.
     *
     * @param n
     */
    public static boolean checkPrimeV2(int n) {
        if (n < 2) {
            return false;
        }
        for (int i = 2; i < n - 1; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
