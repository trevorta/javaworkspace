package javaWorkspace;

import java.util.*;

/**
 * Created by TrevorSF on 3/15/2016.
 */
public class StringManipulation {
    public static void main(String[] args) {
        String h = "Hello world";
        System.out.println(upperCase(h));
        System.out.println(lowerCase(h));
        System.out.println(reverse(h));
        System.out.println(Arrays.toString(split(h)));
        System.out.println(reverseWords(h));
        System.out.println(isPalindrome("hiih"));
        System.out.println(isOneVowel("dfhsdlfjsdkhjdsf"));
        System.out.println(isOneVowel("dfhsdlfjsdkahjdsf"));
        System.out.println(isOneVowel("dfhsdlfaaajsdkhjdsf"));
        System.out.println(translate("figuratively speaking"));
    }


    public static String translate(String s) {
        s = s.toUpperCase();
        Map<Character, Character> map = new HashMap<Character, Character>();
        map.put('T', '7');
        map.put('I', '1');
        map.put('S', '5');
        map.put('E', '3');
        map.put('A', '4');
        map.put('O', '0');

        String res = "";
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (map.containsKey(c)) {
                res += map.get(c);
            } else {
                res += c;
            }
        }
        return res;

    }

    public static boolean isOneVowel(String s) {
        return s.matches("[sdfghjklqwrtypzxcvbnm]*[aiouye][sdfghjklqwrtypzxcvbnm]*");
    }

    public static boolean isPalindrome(String s) {
        return s.equals(reverse(s));
    }

    public static String upperCase(String s) {
        return s.toUpperCase();
    }

    public static String lowerCase(String s) {
        return s.toLowerCase();
    }

    public static String reverse(String s) {
        StringBuilder builder = new StringBuilder(s); // Create a StringBuilder using a String
        // StringBuilders are used to do string manipulation
        return builder.reverse().toString(); // conver the StringBuilder back to the String
    }

    public static String[] split(String s) {
        return s.split(" "); // split the string at space characters
    }

    public static String reverseWords(String s) {
        s = reverse(s); // first, reverse the string s
        System.out.println("Reversed: " + s);
        StringBuilder res = new StringBuilder();
        String[] words = split(s); //split s into individual reversed words
        for (int i = 0; i < words.length; i++) {
            res.append(reverse(words[i]) + " "); // reverse the word and add it back to the result
        }
        return res.toString();
    }
}
