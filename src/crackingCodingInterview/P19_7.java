package crackingCodingInterview;

import java.util.Arrays;

/**
 * Created by TrevorSF on 5/12/2016.
 */
public class P19_7 {
    public static void main(String... args) {
        testResult(new int[] {-2, 3, 4}, 7);
        testResult(new int[] {-2, 3}, 3);
        testResult(new int[] {-2, -1}, 0);
        testResult(new int[] {3,}, 3);
        testResult(new int[] {2, 3, 4}, 9);
        testResult(new int[] {-2, 3, -1}, 3);
    }

    /**
     * Print out the information to make sure that the result is correct
     * @param a: array to find max consecutive sum
     * @param expectedValue: expected value
     */
    public static void testResult(int[] a, int expectedValue) {
        int result = maxConsecutiveSum(a);
        System.out.println("Min of " + Arrays.toString(a) + " : "  + result) ;
        if (expectedValue == result) {
            System.out.println("Correct");
        } else {
            System.out.println("Wrong answer. Expecting " + expectedValue);
        }
    }

    /**
     * The idea is to keep the sum of all the consecutive elements in the array. I f the sum ever drops below zero, cut it off and
     * restart it to zero instead.
     * @param a : array of integers
     * @return the maximum sum of the a consecutive sequence of  elements in A
     */
    public static int maxConsecutiveSum(int[] a) {
        int sum = 0;
        int tempSum = 0;
        for (int i = 0; i < a.length; i++) {
            tempSum += a[i];
            if (tempSum > sum) { // if it's greater, set it to the new sum
                sum = tempSum;
            }
            if (tempSum <= 0) {
                tempSum = 0; // set it to zero again
            }
        }
        return sum;
    }
}
