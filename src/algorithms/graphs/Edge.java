package algorithms.graphs;

/**
 * Represent an edge on the graph. The edge should connect 2 vertices. This edge does not support weighted edges. See
 *
 * @WeightedEdge <p/> Created by TrevorTa on 6/4/16.
 */
public class Edge {
    private Vertex v1; // the first vertex of the edge
    private Vertex v2; // the second vertex of the edge

    /**
     * Constructor for an empty edge. The edge will have 0 weight and null vertices.
     */
    private Edge() {
        v1 = null;
        v2 = null;
    }

    /**
     * Constructor of an edge with 2 vertices. We always require the edge with 2 vertices.
     */
    public Edge(Vertex pv1, Vertex pv2) {
        v1 = pv1;
        v2 = pv2;
    }

}
