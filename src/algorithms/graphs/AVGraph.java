package algorithms.graphs;

import java.util.*;

/**
 * AVGraph stands for ADJACENT VERTICES graph.
 * <p/>
 * Represent a graph with multiple vertices. The edges will be represented by a map from the graph
 * <p/>
 * <p/>
 * Requirements of a graph:
 * <p/>
 * 1. One edge can only have 2 vertices
 * <p/>
 * 2. There can be multiple edges coming out of a vertex
 * <p/>
 * <p/>
 * Created by TrevorTa on 6/4/16.
 */
public class AVGraph {
    private Map<Vertex, List<Vertex>> avMap = new HashMap<Vertex, List<Vertex>>();

    /**
     * Create empty graph.
     */
    public AVGraph() {
        avMap = null;
    }

    /**
     * Initalize the graph with inputs.
     *
     * @param pVertices:
     *         input vertices
     */
    public AVGraph(List<Vertex> pVertices) {
        // TODO
    }

    public void addVertex(Vertex v) {
        //TODO
    }

    public void addEdge(Edge e) {
        //TODO
    }
}
