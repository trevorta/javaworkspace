package crackingCodingInterview;

/**
 * Created by TrevorSF on 4/7/2016.
 */
public class P19_3 {
    public static void main(String... args) {
        for (int i = 1; i < 100; i++) {
            System.out.println("i: " + i + ", i!:" + factorial(i) + ", " + numTrailingZeros(i));
        }
    }
    /**
     * Idea: Count the multiples of 5's. Each time we see a 5 (from 5 or 10), the trailing zeros go up by 1. Account for all power of 5.
     *
     * Count the number of trailing zeros in the product of n factorial.
     * @param n: factorial n
     * @return number of trailing zeros in n!
     */
    public static int numTrailingZeros(int n) {
        int res = 0;
        for (int i = 5; i < n; i *= 5) {
            res += n / 5;
        }
        return res;
    }

    /**
     * Idea: Count the multiples of 5's. Each time we see a 5 (from 5 or 10), the trailing zeros go up by 1. (Wrong!!! Need to account for all power of 5.
     *
     * Count the number of trailing zeros in the product of n factorial.
     * @param n: factorial n
     * @return number of trailing zeros in n!
     */
    public static int numTrailingZerosV2(int n) {
        return n / 5;
    }

    public static long factorial(int n) {
        long prod = 1;
        for (int i = 2; i <= n; i++) {
            prod = prod *i;
        }
        return prod;
    }

    /**
     * This method uses the obvious method of counting the trailing zeros. But this cannot be accurate. **Works only when n < 21
     * Count the number of trailing zeros in the product of n factorial.
     * @param n: factorial n
     * @return number of trailing zeros in n!
     */
    public static int numTrailingZerosV1(int n) {
        long prod = factorial(n);
        int res = 0;
        while (prod % 10 == 0) { // have 0 at the end of the number
            res ++;
            prod /= 10;
        }
        return res;
    }

}
