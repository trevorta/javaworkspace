package algorithms.graphs;

/**
 * The vertext of a point on the graph. This will hold a value of the point.
 * <p/>
 * Created by TrevorTa on 6/4/16.
 */
public class Vertex {
    private int value;
    

    /**
     * Constructor of a vertex.
     *
     * @param v:
     *         The value that the vertex holds.
     */
    public Vertex(int v) {
        value = v;
    }

    /**
     * Constructor for an empty vertex. The value supposes value of 0.
     */
    public Vertex() {
        value = 0;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }


}
