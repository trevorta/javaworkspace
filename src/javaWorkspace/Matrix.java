package javaWorkspace;

import java.util.*;

/**
 * Created by TrevorTa on 3/22/16.
 */
public class Matrix {
    public static void main(String[] args) {
        int[][] a1 = {
                {0, 0, 0, 1, 0, 1},
                {2, 0, 0, 1, 1, 0},
                {1, 2, 0, 1, 0, 0},
                {2, 0, 1, 1, 0, 0},
                {1, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 2, 0}
        };
        System.out.println("The winner for a1 is " + checkCaro(a1));

        int[][] a2 = {
                {0, 0, 0, 1, 0, 1},
                {2, 0, 0, 1, 1, 0},
                {1, 2, 0, 1, 0, 0},
                {2, 0, 0, 1, 0, 0},
                {1, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 2, 0}
        };
        System.out.println("The winner for a2 is " + checkCaro(a2)); // should be 0

        int[][] a3 = {
                {0, 0, 0, 1, 0, 1},
                {2, 0, 0, 1, 1, 0},
                {1, 2, 0, 1, 0, 0},
                {2, 0, 2, 1, 0, 0},
                {1, 1, 0, 2, 0, 0},
                {1, 0, 0, 1, 2, 0}
        };
        System.out.println("The winner for a3 is " + checkCaro(a3)); // should be 2

        int[][] a4 = {
                {0, 0, 0, 1, 0, 1},
                {2, 0, 0, 1, 1, 0},
                {1, 2, 0, 1, 0, 0},
                {2, 0, 1, 1, 0, 0},
                {1, 2, 0, 2, 0, 0},
                {1, 0, 0, 1, 2, 0}
        };
        System.out.println("The winner for a4 is " + checkCaro(a4)); // should be 0

        int[][] a5 = {
                {0, 0, 0, 1, 0, 1},
                {2, 0, 2, 1, 1, 0},
                {1, 2, 0, 1, 0, 0},
                {2, 0, 2, 1, 0, 0},
                {1, 1, 0, 1, 0, 0},
                {1, 0, 0, 1, 2, 0}
        };
        System.out.println("The winner for a5 is " + checkCaro(a5)); // should be 1

    }


    /**
     * Return the winner of the board B. The winner of a caro game has 5 consecutives on either one row, one column, or
     * one diagonal
     *
     * @param B:
     *         caro board, empty spots are represented by 0, player1's are 1 and player2's are 2
     *
     * @return
     */
    public static int checkCaro(int[][] B) {
        for (int r = 0; r < B.length; r++) {
            for (int c = 0; c < B[0].length; c++) {
                if (B[r][c] > 0) { // there must be an item here; cannot be empty (value of 0)
                    // if one if the condition is satisfied, return the winning player
                    if (checkCaroColumn(B, r, c)
                                || checkCaroLeftDiagonal(B, r, c)
                                || checkCaroRightDiagonal(B, r, c)
                                || checkCaroRow(B, r, c)) {
                        return B[r][c];
                    }
                }
            }
        }
        return 0;
    }

    /**
     * Check if the left diagonal that starts at this position (r,c) is won.
     *
     * @param B:
     *         board
     * @param r:
     *         row index
     * @param c:
     *         column index
     *
     * @return true if the diagonal wins
     */
    public static boolean checkCaroLeftDiagonal(int[][] B, int r, int c) {
        if (!validPosition(B, r, c) || !validPosition(B, r - 4, c + 4)) { // if the entire diagonal is not valid,
            return false;
        }
        // FOR THE LEFT DIAGONAL, THE ROW INDEX DECREASES AS WE GO
        for (int i = 1; i < 5; i++) {
            if (B[r - i][c + i] != B[r][c]) { // if the value on the diagonal is not the same as the first item
                return false;
            }
        }

        // if all elems are the same, return true
        return true;
    }


    /**
     * Check if the right diagonal that starts at this position (r,c) is won.
     *
     * @param B:
     *         board
     * @param r:
     *         row index
     * @param c:
     *         column index
     *
     * @return true if the diagonal wins
     */
    public static boolean checkCaroRightDiagonal(int[][] B, int r, int c) {
        if (!validPosition(B, r, c) || !validPosition(B, r + 4, c + 4)) { // if the entire diagonal is not valid,
            return false;
        }
        for (int i = 1; i < 5; i++) {
            if (B[r + i][c + i] != B[r][c]) { // if the value on the diagonal is not the same as the first item
                return false;
            }
        }

        // if all elems are the same, return true
        return true;
    }

    /**
     * Check if the column that starts at this position (r,c) is won.
     *
     * @param B:
     *         board
     * @param r:
     *         row index
     * @param c:
     *         column index
     *
     * @return true if the column wins
     */
    public static boolean checkCaroColumn(int[][] B, int r, int c) {
        if (!validPosition(B, r, c) || !validPosition(B, r, c + 4)) { // if the entire column is not valid, return false
            return false;
        }
        for (int i = c + 1; i < c + 5; i++) {
            if (B[r][i] != B[r][c]) { // if the value on the column is not the same as the first item on the column,
                return false;
            }
        }
        // if all elems are the same, return true
        return true;
    }

    /**
     * Check if the row that starts at this position (r,c) is won.
     *
     * @param B:
     *         board
     * @param r:
     *         row index
     * @param c:
     *         column index
     *
     * @return true if the row wins
     */
    public static boolean checkCaroRow(int[][] B, int r, int c) {
        if (!validPosition(B, r, c) || !validPosition(B, r + 4, c)) { // if the entire row is not valid, return false
            return false;
        }
        for (int i = r + 1; i < r + 5; i++) {
            if (B[i][c] != B[r][c]) { // if the value on the row is not the same as the first item on the row,
                return false;
            }
        }
        // if all elems are the same, return true
        return true;
    }

    /**
     * Check if the position (r, c) is within the board.
     *
     * @param B:
     *         board
     * @param r:
     *         row index
     * @param c:
     *         column index
     *
     * @return true if (r, c) is valid, false otherwise
     */
    public static boolean validPosition(int[][] B, int r, int c) {
        // check if r and c are within the number of rows and columns
        if (r >= 0 && r < B.length && c >= 0 && c < B[0].length) {
            return true;
        }
        return false;
    }

    public static void test1() {
        int[][] a = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        printMatrix(a);
        int[][] b = {{5, 5}, {5, 9}, {5, 1}};
        printMatrix(b);
        System.out.println(repeatColumn(b));
    }

    /**
     * Check if matrix M contains a column of all same value.
     *
     * @param M:
     *         integer matrix
     *
     * @return true if M contains a column of the same value
     */
    public static boolean repeatColumn(int[][] M) {
        for (int c = 0; c < M[0].length; c++) { // iterate through each column
            Set<Integer> row = new HashSet<Integer>();
            // This is a new concept: a Set is like a List, but it doesn't have repeated values. If a value already
            // exists in the list, it just automatically not add it.
            for (int r = 0; r < M.length; r++) { // iterate through the row
                row.add(M[r][c]); // add all values for each column
            }
            if (row.size() == 1) { // if the row contains only 1 value (set doesn't have repeated value), return true
                return true;
            }
        }
        return false;
    }

    /**
     * Add 2 matrices of different sizes.
     *
     * @param a
     * @param b
     *
     * @return
     */
    public static int[][] addDifferentMatrix(int[][] a, int[][] b) {
        int row = Math.max(a.length, b.length);
        int col = Math.max(a[0].length, b[0].length);
        int[][] res = new int[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                res[i][j] = getItemFromMatrix(a, i, j) + getItemFromMatrix(b, i, j);
            }
        }
        return res;
    }

    /**
     * Get item from matrix M at row R and column C. Return 0 for illegal position.
     *
     * @param m
     * @param r
     * @param c
     *
     * @return
     */
    public static int getItemFromMatrix(int[][] m, int r, int c) {
        if (r < m.length && c < m[0].length) {
            return m[r][c];
        } else {
            return 0;
        }
    }

    /**
     * Print the matrix A in human-readable form.
     *
     * @param a:
     *         integer matrix
     */
    public static void printMatrix(int[][] a) {
        System.out.println("================");
        for (int r = 0; r < a.length; r++) { // iterate through each row
            for (int c = 0; c < a[0].length; c++) {
                //System.out.print(a[r][c] + " "); // print the value at each row and column
                System.out.print(itemFormat2(a[r][c]));
            }
            System.out.println(); // print the new line after each row
        }
        System.out.println("================");
    }

    /**
     * @param i:
     *         integer
     *
     * @return the normal string of one item in one row of a matrix. Return "1 ", "2 ", ...
     */
    public static String itemFormat1(int i) {
        return i + " ";
    }

    /**
     * @param i:
     *         integer
     *
     * @return the string PADDING LEFT 2 spaces. Return "1  12 13 1  3  ", ...
     */
    public static String itemFormat2(int i) {
        return String.format("%2d ", i);
    }


    /**
     * @param i:
     *         integer
     *
     * @return the string PADDING RIGHT 2 spaces. Return "1  12 13 1  3  ", ...
     */
    public static String itemFormat3(int i) {
        return String.format("%-2d ", i);
    }

    /**
     * @param i:
     *         integer
     *
     * @return the string PADDING LEFT WITH 2 ZEROS. Return "1  12 13 1  3  ", ...
     */
    public static String itemFormat4(int i) {
        return String.format("%02d ", i);
    }

    /**
     * Return a matrix that is the sum of A and B. (We assume that a and b have the same dimension)
     *
     * @param a:
     *         integer matrix
     * @param b:
     *         integer matrix
     */
    public static int[][] addMatrix(int[][] a, int[][] b) {
        int[][] R = new int[a.length][a[0].length]; // create a new matrix that has that same dimension as matrix A
        // a.length: the number of rows of a
        // a[0]: The first row of a
        // a[0].length: the number of columns of a
        for (int r = 0; r < a.length; r++) { // r is the row index; iterate through all the rows of A
            for (int c = 0; c < a[0].length; c++) { // c is the column index; iterate through all the columns of A
                R[r][c] = a[r][c] + b[r][c];
            }
        }
        return R;
    }


    /**
     * Negate all elements in A.
     *
     * @param a:
     *         integer matrix
     *
     * @return A for chaining purposes
     */
    public static int[][] inverseMatrix(int[][] a) {
        for (int r = 0; r < a.length; r++) {
            for (int c = 0; c < a[0].length; c++) {
                a[r][c] = -a[r][c];
            }
        }
        return a;
    }

    /**
     * @param a
     *         : integer matrix
     * @param b
     *         : integer matrix
     *
     * @return the result of matrix A subtracts B.
     */
    public static int[][] substractMatrix(int[][] a, int[][] b) {
        return addMatrix(a, inverseMatrix(b));
    }

    /**
     * @param a:
     *         integer matrix
     * @param b:
     *         integer matrix
     *
     * @return the product of matrix multiplication of A and B
     */
    public static int[][] multiplyMatrix(int[][] a, int[][] b) {
        // TODO
        return null;
    }

    /**
     * @param a:
     *         integer matrix
     *
     * @return flip matrix a across the y-axis (flip horizontally)
     */
    public static int[][] flipMatrixHorizontally(int[][] a) {
        //TODO
        return null;
    }

    /**
     * @param a:
     *         integer matrix
     *
     * @return flip matrix a across the y-axis (flip vertically)
     */
    public static int[][] flipMatrixVertically(int[][] a) {
        //TODO
        return null;
    }

    /**
     * @param a:
     *         integer matrix
     *
     * @return rotate matrix A 90 degrees to the left
     */
    public static int[][] rotateMatrix(int[][] a) {
        //TODO
        return null;
    }


}
