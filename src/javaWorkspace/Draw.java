package javaWorkspace;

/**
 * Created by TrevorTa on 3/22/16.
 */
public class Draw {
    public static void main(String[] args) {
        drawRectangle(4, 3);
        drawSquare(5);
    }

    public static void drawSquare(int n) {
        drawRectangle(n, n);
    }

    public static void drawRectangle(int r, int c) {
        String s = repeat("* ", r); // create a row
        String rect = repeat(s + "\n", c); // repeat c times, "\n" is the next line character
        System.out.println(rect);
    }

    /**
     * @param s
     * @param n
     *
     * @return result of repeating string s n times.
     */
    public static String repeat(String s, int n) {
        String res = "";
        for (int i = 0; i < n; i++) {
            res += s;
        }
        return res;
    }
}
