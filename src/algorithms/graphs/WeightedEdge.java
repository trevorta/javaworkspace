package algorithms.graphs;

/**
 * Represent an edge on the graph. The edge should connect 2 vertices and has an optional weight.
 * <p/>
 * Created by TrevorTa on 6/4/16.
 */
public class WeightedEdge extends Edge {
    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    private int weight; // the optional weight of the edge
    private Vertex v1; // the first vertex of the edge
    private Vertex v2; // the second vertex of the edge

    /**
     * Constructor of an edge.
     *
     * @param w:
     *         The weight of the edge.
     */
    public WeightedEdge(Vertex v1, Vertex v2, int w) {
        super(v1, v2);
        weight = w;
    }

    /**
     * Constructor for an empty edge. The edge will have 0 weight and null vertices.
     */
    public WeightedEdge(Vertex v1, Vertex v2) {
        super(v1, v2);
        weight = 0;
    }

}
