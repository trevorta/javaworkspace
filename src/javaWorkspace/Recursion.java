package javaWorkspace;


import java.util.*;

/**
 * Created by TrevorTa on 3/22/16.
 */
public class Recursion {
    public static void main(String... args) {
        int[] a = {1, 2, 3, 4, 6, 7, 9};
        int[] b = {3, 5, 6, 8, 9, 11, 154};
        System.out.println(Arrays.toString(merge(a, b)));
        System.out.println("I have finished them all!");
    }

    public static void test1() {
        System.out.println("Hello world");
        printDownwardTriangle(5);
        System.out.println();
        printUpwardTriangle(5);
        System.out.println();
    }

    /**
     * Merge 2 sorted arrays a and b together and return the resulting array.
     *
     * @param a:
     *         sorted array to be merged
     * @param b:
     *         sorted array to be merge
     *
     * @return new sorted array that is the result of merging array a and b
     */
    public static int[] merge(int[] a, int[] b) {
        if (a == null || a.length == 0) {
            return b;
        }
        if (b == null || b.length == 0) {
            return a;
        }
        Arrays.sort(a);
        Arrays.sort(b);
        return merge(a, b, 0, 0, new int[a.length + b.length]);
    }

    // Helper recursive version of merge(int[] a, int[] b)
    public static int[] merge(int[] a, int[] b, int ia, int ib, int[] t) {
        if (ia >= a.length && ib >= b.length) {
            return t;
        }
        if (ia >= a.length) {
            t[ia + ib] = b[ib];
            return merge(a, b, ia, ib + 1, t);
        }
        if (ib >= b.length) {
            t[ia + ib] = a[ia];
            return merge(a, b, ia + 1, ib, t);
        }
        // Assign the smaller item to the result array
        if (a[ia] < b[ib]) {
            t[ia + ib] = a[ia];
            return merge(a, b, ia + 1, ib, t);
        } else {
            t[ia + ib] = b[ib];
            return merge(a, b, ia, ib + 1, t);
        }
    }

    /**
     * Print a downward triangle of length n
     *
     * @param n:
     *         length of a side of the triangle
     */
    public static void printUpwardTriangle(int n) {
        if (n <= 0) {
            return;
        }
        printUpwardTriangle(n - 1);
        printLine(n);
    }

    /**
     * Print a triangle of *'s with a side of length n.
     *
     * @param n:
     *         length of a side of a right triangle.
     */
    public static void printDownwardTriangle(int n) {
        if (n <= 0) {
            return;
        }
        printLine(n);
        printDownwardTriangle(n - 1);
    }

    /**
     * Print n *'s on a line.
     *
     * @param n:
     *         number of stars to print on a line
     */
    public static void printLine(int n) {
        if (n <= 0) {
            System.out.println();
        } else {
            System.out.print("* ");
            printLine(n - 1);
        }
    }
}
