package crackingCodingInterview;

import java.util.*;

/**
 * Created by TrevorSF on 4/6/2016.
 */
public class P19_2 {
    public static void main(String... args) {
        int[][] M = {{2, 1, 2}, {1, 2, 3}, {3, 4, 2}};
        System.out.println(checkTicTacToe(M));
        System.out.println(checkTicTacToeV1(M));
    }

    /**
     * This method uses a hash map to keep the values of each rows, columns, and diagonals and check if  at the end the set
     * has one element. The method only iterates through the matrix once.
     *
     * Check if the matrix has a winning move of the player represented as an integer in the matrix.
     * @param M: the board (assume that the board is a well-formed 3x3 tic tac toe board.
     *          _ 0 1 2
     *         0 1  1 2
     *         1 2 2 1
     *         2 1 2 1
     *
     * @return true if the board has a winner
     */
    public static boolean checkTicTacToeV1(int[][] M) {
        // each tile has a character.
        // put the row index from 0 to 2
        // put the column index from 3 to 5
        // put the diagonals to 6 and 7
        Map<Integer, Set<Integer>> elemMap = new HashMap<Integer, Set<Integer>>();
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                int t = M[r][c]; // the item at (r, c)
                // add to the row
                if (elemMap.containsKey(r)) { // the row
                    elemMap.get(r).add(t); // add t to the set
                } else {
                    Set<Integer> set = new HashSet<Integer>();
                    set.add(t); // add t to the new set
                    elemMap.put(r, set); // add the mapping to the set
                }
                // add to the column
                if (elemMap.containsKey(3 + c)) { // the column, offset by 3
                    elemMap.get(3 + c).add(t); // add t to the set
                } else {
                    Set<Integer> set = new HashSet<Integer>();
                    set.add(t); // add t to the new set
                    elemMap.put(3 + c, set); // add the mapping to the set
                }
                // add to the diagonal
                if (r == c) { // left diagonal
                    if (elemMap.containsKey(6)) { // the diagonal
                        elemMap.get(6).add(t); // add t to the set
                    } else {
                        Set<Integer> set = new HashSet<Integer>();
                        set.add(t); // add t to the new set
                        elemMap.put(6, set); // add the mapping to the set
                    }
                }

                // add to the right diagonal
                if (2 - r == c) { // left diagonal
                    if (elemMap.containsKey(7)) { // the diagonal
                        elemMap.get(7).add(t); // add t to the set
                    } else {
                        Set<Integer> set = new HashSet<Integer>();
                        set.add(t); // add t to the new set
                        elemMap.put(7, set); // add the mapping to the set
                    }
                }
            }
        }

        for (Set<Integer> set: elemMap.values()) {
            if (set.size() == 1) { // if the set only contains 1 element, then that row/column/diagonal has winning move
                return true;
            }
        }
        return false;
    }

    /**
     * Check if the matrix has a winning move of the player represented as an integer in the matrix.
     * @param M: the board (assume that the board is a well-formed 3x3 tic tac toe board.
     *          _ 0 1 2
     *         0 1  1 2
     *         1 2 2 1
     *         2 1 2 1
     *
     * @return true if the board has a winner
     */
    public static boolean checkTicTacToe(int[][] M) {
        for (int r = 0; r < 3; r++) {
            if (checkRow(M, r)) {
                return true;
            }
        }

        for (int c = 0; c < 3; c++) {
            if (checkColumn(M, c)) {
                return true;
            }
        }

        return checkLeftDiagonal(M) || checkRightDiagonal(M);
    }

    /**
     * Check if this row @param r in matrix M has the winning move.
     * @param M: matrix
     * @param r: row
     * @return: true if this row R of matrix M are the same.
     */
    public static boolean checkRow(int[][] M, int r) {
        int t = M[r][0];
        for (int c= 0; c < 3; c++) {
            if (t != M[r][c]) { // if not equals, then this is wrong, return false
                return false;
            }
        }
        return true;
    }

    /**
     * @param M : matrix
     * @param c : column
     * @return true if this column @param c in matrix M has the winning move.
     */
    public static boolean checkColumn(int[][] M, int c) {
        int t = M[0][c];
        for (int r= 0; r < 3; r++) {
            if (t != M[r][c]) { // if not equals, then this is wrong, return false
                return false;
            }
        }
        return true;
    }

    /**
     * @param M : matrix
     * @return true if  the diagonal starts from the top left down of M contain a winning move
     */
    public static boolean checkLeftDiagonal(int[][] M) {
        int t1 = M[0][0];
        for (int r = 1; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                if (r == c && t1 != M[r][c]) {// if the element is not the same as the other one, break out
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param M : matrix
     * @return true if  the diagonal starts from the top right down of M contain a winning move
     */
    public static boolean checkRightDiagonal(int[][] M) {
        int t1 = M[0][2];
        for (int r = 1; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                if (2 - r == c && t1 != M[2 - r][c]) {// if the element is not the same as the other one, break out
                    return false;
                }
            }
        }
        return true;
    }
}
